import React from "react";
import ReactDOM from "react-dom/client";
import App from "src/App";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "src/core/theme/theme";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </React.StrictMode>
);
