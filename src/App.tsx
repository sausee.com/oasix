import Layout from "src/components/Layout";
import AppRoutes from "src/components/AppRoutes";
import { BrowserRouter as Router } from "react-router-dom";

import "./styles/global.scss";

function App() {
  return (
    <Router>
      <Layout>
        <AppRoutes />
      </Layout>
    </Router>
  );
}

export default App;
