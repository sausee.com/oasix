import React, { useEffect, useState } from "react";
import styles from "src/components/Parallax/Parallax.module.scss";

const Parallax = () => {
  const images = Array.from(
    { length: 64 },
    (_, index) =>
      `https://www.apple.com/105/media/us/airpods-pro/2022/d2deeb8e-83eb-48ea-9721-f567cf0fffa8/anim/hero/large/${String(
        index + 1
      ).padStart(4, "0")}.png`
  );
  

  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const [lastScrollTop, setLastScrollTop] = useState(0);

  useEffect(() => {
    images.forEach((imageSrc) => {
      const img = new Image();
      img.src = imageSrc;
    });
  }, [images]);

  useEffect(() => {
    const handleScroll = () => {
      const scrolled = window.scrollY;

      if (scrolled > lastScrollTop) {
        if (scrolled > 688) {
          setCurrentImageIndex((prevIndex) =>
            Math.min(prevIndex + 1, images.length - 1)
          );
        }
      } else {
        setCurrentImageIndex((prevIndex) => Math.max(prevIndex - 1, 0));
      }

      setLastScrollTop(scrolled);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [currentImageIndex, images, lastScrollTop]);

  return (
    <div className={`${styles.parallaxContainer} parallax-container`}>
      <div
        className={styles.parallaxImage}
        style={{ backgroundImage: `url(${images[currentImageIndex]})` }}
      ></div>
    </div>
  );
};

export default Parallax;
