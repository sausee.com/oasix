import styles from "src/components/Layout/Footer/Footer.module.scss";
function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.footerSection}>
        <div className={styles.contactInfo}>
          <span>Need Live Support? +1(256)-297-82-14</span>
          <span>hello@oasix.ca</span>
        </div>
        <div className={styles.apps}>
          <a href="https://apple.com" target="_blank" rel="noopener noreferrer">
            Download on the Apple Store
          </a>
          <a
            href="https://play.google.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Get it on Google Play
          </a>
        </div>
        <div className={styles.socialMedia}>
        </div>
      </div>
      <div className={styles.footerSection}>
        <div className={styles.newsletter}>
          <input type="email" placeholder="Your Email" />
          <button>Subscribe</button>
        </div>
        <div className={styles.links}></div>
      </div>
      <div className={styles.footerSection}>
        <div className={styles.discover}>
        </div>
      </div>
      <div className={styles.copyRight}>
        <span>© 2023 OASIX - All rights reserved</span>
      </div>
    </footer>
  );
}

export default Footer;
