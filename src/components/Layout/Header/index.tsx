import styles from "src/components/Layout/Header/Header.module.scss";
import logo from "src/resourses/images/logo.png";
import menu from "src/resourses/icons/icon-menu.svg";

const Header = () => {

  return (
    <header className={styles.header}>
      <div className={styles.headerLeft}>
        <img src={logo} alt="Логотип" className={styles.logo} />
      </div>
      <div className={styles.headerRight}>
        <div className={styles.burgerMenu}>
          <img src={menu} alt="menu" />
        </div>
      </div>
    </header>
  );
};

export default Header;
