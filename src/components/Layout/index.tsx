import React from "react";
import Header from "src/components/Layout/Header";
import Footer from "src/components/Layout/Footer";

type LayoutProps = {
  children: React.ReactNode;
};

function Layout({ children }: LayoutProps) {
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  );
}

export default Layout;
