import { Box, TextField } from "@mui/material";
import ListImage from "src/resourses/images/map-list-item.png";
import React from "react";

function MapListItems() {
  return (
    <Box>
      <img src={ListImage} alt="list-image" />
      <Box color={"#FFF"} fontSize="24px">335-AB Apartments</Box>
    </Box>
  );
}

export default MapListItems;
