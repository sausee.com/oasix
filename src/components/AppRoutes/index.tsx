import { Route, Routes } from "react-router-dom";
import MainPage from "src/pages/MainPage";
import MapPage from "src/pages/MapPage";

function AppRoutes() {
  return (
    <Routes>
      <Route path="/" element={<MainPage />} />
      <Route path="/map" element={<MapPage />} />
    </Routes>
  );
}

export default AppRoutes;
