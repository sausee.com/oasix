import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#FFF",
    },
    secondary: {
      main: "#FF5722",
    },

  },
  components: {
    MuiAutocomplete: {
      styleOverrides: {
        inputRoot: {
          backgroundColor: "transparent",
          border: "none",
          fieldset: {
            border: "none",
          },
          ".MuiAutocomplete-endAdornment": {
            display: "none",
          },
          paddingRight: "0px!important",
        },
        input: {
          borderRadius: "42.521px",
          border: "1.276px solid rgba(0, 0, 0, 0.00)",
          background: "#FFFFFF",
          boxShadow: "0px 3.40167px 3.40167px 0px rgba(61, 61, 61, 0.35) inset",
          backdropFilter: "blur(42.52089309692383px)",
          padding: "24px 30px!important",
          color: "#000000!important",
        },
        listbox: {
          border: "1.276px solid rgba(0, 0, 0, 0.00)",
          boxShadow: "0px 3.40167px 3.40167px 0px rgba(61, 61, 61, 0.35) inset",
          backdropFilter: "blur(42.52089309692383px)",
          overflow: "hidden"
        },
        option: {
          '&[aria-selected="true"]': {},
        },
      },
    },
    MuiGrid: {
      styleOverrides: {
        root: {
          display: "flex",
          flexDirection: "column",
          borderRadius: "52.8px",
          background: "linear-gradient(143deg, rgba(255, 255, 255, 0.37) -3.54%, rgba(114, 114, 114, 0.42) 95.15%)",
          padding:"0 28px",
          width: "fit-content",

          ":after": {
            borderBottom:"none!important",
          },

          "h3": {
            fontFamily: "Avenir Next",
            fontSize: "30px",
            fontStyle: "normal",
            fontWeight: "600",
            lineHeight: "normal",
            marginTop: "35px",
            color: "#FAF9F5"
          },
        },

      },
    },
    MuiInput:{
      styleOverrides: {
        root:{
          
        },
        input:{
          borderRadius: "20px",
          border: "1.276px solid rgba(0, 0, 0, 0.00)",
          background: "rgba(0, 0, 0, 0.15)",
          boxShadow: "0px 3.40167px 3.40167px 0px rgba(61, 61, 61, 0.35) inset",
          backdropFilter: "blur(42.52089309692383px)",
          padding: "17px",
          color: "#FFF",
          fontFamily: "Avenir",
          fontSize: "14.457px",
          fontStyle: "normal",
          fontWeight: "500",
          lineHeight: "18.709px",
          letterSpacing: "-0.072px"
        }
      }
    },
  },
});
