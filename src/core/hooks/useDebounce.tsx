import { useState, useEffect, SetStateAction } from 'react';

function useDebounce(callback: (e: any) => void, delay: number) {
  const [value, setValue] = useState("");

  useEffect(() => {
    const timer = setTimeout(() => {
      if (value !== "") {
        callback(value);
      }
    }, delay);

    return () => {
      clearTimeout(timer);
    };
  }, [value, callback, delay]);

  return (newValue: string) => {
    setValue(newValue);
  };
}

export default useDebounce;
