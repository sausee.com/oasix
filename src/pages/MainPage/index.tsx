import React, { useEffect, useState, useRef } from "react";
import { Element } from "react-scroll";
import _throttle from "lodash/throttle";
import styles from "src/pages/MainPage/MainPage.module.scss";
import MainPageTitle from "src/components/MainPageTitle";

function MainPage() {
  const [isScrolled, setIsScrolled] = useState(false);
  const totalImages = 204;
  const imageUrls = Array.from({ length: totalImages }, (_, index) =>
    require(`../../resourses/images/f60_q70/1-${index + 1}.jpg`)
  );
  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const [lastScrollTop, setLastScrollTop] = useState(0);

  const handleScroll = () => {
    const scrolled = window.scrollY;
    const scrollThreshold = 583;

    if (scrolled > lastScrollTop && scrolled > scrollThreshold) {
      setIsScrolled(true);
      setCurrentImageIndex((prevIndex) =>
        Math.min(prevIndex + 1, totalImages - 1)
      );
    } else if (scrolled > scrollThreshold) {
      setCurrentImageIndex((prevIndex) => Math.max(prevIndex - 1, 0));
    } else if (scrolled === 0) {
      setIsScrolled(false);
      setCurrentImageIndex(0);
    }

    setLastScrollTop(scrolled);
  };

  const throttledHandleScroll = _throttle(handleScroll, 1);
  useEffect(() => {
    window.addEventListener("scroll", throttledHandleScroll);

    return () => {
      window.removeEventListener("scroll", throttledHandleScroll);
    };
  }, [throttledHandleScroll]);
  const requestRef = useRef<any>();
  const previousTimeRef = useRef();

  const animate = (time:any) => {
    if (previousTimeRef.current !== undefined) {
      const deltaTime = time - previousTimeRef.current;
      if (deltaTime >= 600) {
        handleScroll();
      }
    }
    previousTimeRef.current = time;
    requestRef.current = requestAnimationFrame(animate);
  };

  useEffect(() => {
    const preloadImages = () => {
      imageUrls.forEach((imageSrc) => (new Image().src = imageSrc));
    };

    preloadImages();
  }, [imageUrls]);

  useEffect(() => {
    const handleScrollWithRAF = () => {
      if (!requestRef.current) {
        requestRef.current = requestAnimationFrame((time) => {
          animate(time);
          requestRef.current = null;
        });
      }
    };

    window.addEventListener("scroll", handleScrollWithRAF);

    return () => {
      window.removeEventListener("scroll", handleScrollWithRAF);
      cancelAnimationFrame(requestRef.current);
    };
  }, []);

  return (
    <>
      <main className={styles.main}>
        <Element name="mainBg" className={styles.mainBg}>
          {isScrolled && <img src={imageUrls[currentImageIndex]} alt="" />}
        </Element>
        <MainPageTitle isVisibleCard={currentImageIndex >= 32 && currentImageIndex <= 50} isActiveBuild={currentImageIndex >= 60 && currentImageIndex <= 39 * 2}/>
      </main>
    </>
  );
}

export default MainPage;
