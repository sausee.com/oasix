import { Box, Grid, Input } from "@mui/material";
import React from "react";
import MapListItems from "src/components/MapListItems";
import styles from "src/pages/MapPage/MapPage.module.scss";
import FilterIcon from "src/resourses/icons/icon-filter.svg";

function MapPage() {
  return (
    <main className={styles.main}>
      <Box margin={"0 auto"} maxWidth={"1000px"}>
        <Grid>
          <h3>Property listing</h3>
          <Box display={"flex"} gap={"13px"} marginBottom={"11px"}>
            <Input placeholder="Search" />
            <img src={FilterIcon} alt="filter-icon" />
          </Box>
          <Box maxHeight={"350px"} overflow={"auto"}>
            {[...new Array(2)].map((el, index) => (
              <React.Fragment key={index}>
                <MapListItems />
              </React.Fragment>
            ))}
          </Box>
        </Grid>
      </Box>
    </main>
  );
}

export default MapPage;

// footer